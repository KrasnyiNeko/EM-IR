# EM-IR
## Getting Started
Clone repo and open up the directory in PlatformIO. It should download the needed libs. User options can be set in the config.h file. The only things that would need to be changed if using a node board are the pins used for the receiver/transmitter. Some more changes may need to be made is using an esp01 due to the pin limitations as some of the gpio pins are used for serial rx/tx. Future releases will cleanup some code and make the device more user friendly.

## Commands
### Send IR Command
The following command takes a json payload

`cmnd/[DEVICE_NAME]/ir`

Sample json

```
{
  "encoding": "NEC",
  "data": "20df40bf",
  "bits": "32"
}
```

## Receive IR Command
An easy way to get the needed json is to open an mqtt client that can send/receive such as [MQTTFX](http://mqttfx.jfx4ee.org/). You will have to first enable ir listening. The command for that is below. Subscribe to the following topic

`data/[DEVICE_NAME]`

```
{
  "encoding": "NEC",
  "data": "20df40bf",
  "bits": "32"
}
```

## Enable IR listening
The device on start up will start with listening off. If the following topic is published with out any data it will toggle it, turning it on. Atm there is no way to know if it is listening without pointing a remote to the device and see if it publishes. If for some reason you would like to have the device automatically start listening on startup, publish the following topic with the retain flag. This is untested, though in the future better configuration options will be available.

`cmnd/[DEVICE_NAME]/config`
