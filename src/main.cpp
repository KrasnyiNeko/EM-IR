#include <Arduino.h>
#include <ArduinoOTA.h>
#include <ESP8266mDNS.h>
#include <FS.h>
#include <Hash.h>

#include <ESP8266WiFi.h>
#include <AsyncMqttClient.h>
#include <IRremoteESP8266.h>
#include <ArduinoJson.h>
#include <DNSServer.h>
#include <ESPAsyncWebServer.h>
#include <ESPAsyncWiFiManager.h>
#include <Button.h>

#include <config.h>

AsyncMqttClient mqttClient;
decode_results results;

IRrecv irRecv(RECV_PIN);
IRsend irSend(TRAN_PIN);
Button button(BUTTON_PIN, false, true, 20);
// Device
bool listen = true;
char device_name[16] = DEVICE;
bool shouldSaveConfig = false;

// MQTT
char mqtt_server[64] = "example.com";
char mqtt_port[8] = "1883";
char mqtt_username[64];
char mqtt_password[64];
char sub_prefix[8] = SUB_PREFIX;
char pub_prefix[8] = PUB_PREFIX;
char ir_endpoint[8] = IR_ENDPOINT;
char config_endpoint[8] = CONFIG_ENDPOINT;
char irTopic[32];
char cfTopic[32];
char pbTopic[32];

// server
AsyncWebServer server(80);
DNSServer dns;

void saveConfigCallback(){
  Serial.println("save config flag set too true");
  shouldSaveConfig = true;
}

void  encodingToJson(decode_results *results, JsonObject& json){
  switch (results->decode_type) {
    default:
    case UNKNOWN:      json["encoding"] = "UNKNOWN";        break ;
    case NEC:          json["encoding"] = "NEC";            break ;
    case SONY:         json["encoding"] = "SONY";           break ;
    case RC5:          json["encoding"] = "RC5";            break ;
    case RC6:          json["encoding"] = "RC6";            break ;
    case DISH:         json["encoding"] = "DISH";           break ;
    case SHARP:        json["encoding"] = "SHARP";          break ;
    case JVC:          json["encoding"] = "JVC";            break ;
    case SANYO:        json["encoding"] = "SANYO";          break ;
    case MITSUBISHI:   json["encoding"] = "MITSUBISHI";     break ;
    case SAMSUNG:      json["encoding"] = "SAMSUNG";        break ;
    case LG:           json["encoding"] = "LG";             break ;
    case WHYNTER:      json["encoding"] = "WHYNTER";        break ;
    case PANASONIC:    json["encoding"] = "PANASONIC";      break ;
  }
}

int jsonToEncoding(JsonObject& json){

  if (strcmp(json["encoding"], "UNKNOWN")    == 0) return UNKNOWN;
  if (strcmp(json["encoding"], "NEC")        == 0) return NEC;
  if (strcmp(json["encoding"], "SONY")       == 0) return SONY;
  if (strcmp(json["encoding"], "RC5")        == 0) return RC5;
  if (strcmp(json["encoding"], "RC6")        == 0) return RC6;
  if (strcmp(json["encoding"], "DISH")       == 0) return DISH;
  if (strcmp(json["encoding"], "SHARP")      == 0) return SHARP;
  if (strcmp(json["encoding"], "JVC")        == 0) return JVC;
  if (strcmp(json["encoding"], "SANYO")      == 0) return SANYO;
  if (strcmp(json["encoding"], "MITSUBISHI") == 0) return MITSUBISHI;
  if (strcmp(json["encoding"], "SAMSUNG")    == 0) return SAMSUNG;
  if (strcmp(json["encoding"], "LG")         == 0) return LG;
  if (strcmp(json["encoding"], "WHYNTER")    == 0) return WHYNTER;
  if (strcmp(json["encoding"], "PANASONIC")  == 0) return PANASONIC;
  return -1;
}

void onConnected(bool sessionPresent){
  Serial.print("Connected to mqtt server on: ");
  Serial.println(mqtt_server);
  mqttClient.subscribe(irTopic, 0);
  mqttClient.subscribe(cfTopic, 0);
}

void onMessage(char topic[], char payload[], AsyncMqttClientMessageProperties properties, size_t len, size_t index, size_t total) {
  // Toggle ir recv
  // TODO: add config options
  if (!strcmp(topic, cfTopic)) listen = !listen;

  if (!strcmp(topic, irTopic)){
    DynamicJsonBuffer jsonBuffer;
    JsonObject& root = jsonBuffer.parseObject(payload);
    if (!root.containsKey("encoding") or !root.containsKey("data") or !root.containsKey("bits")){
      Serial.println("Missing keys or malformed json detected. Please check json.");
      return;
    }else{
      irSend.send(jsonToEncoding(root), strtoul(root["data"], 0, 16), atoi(root["bits"]));
    }
  }
}

void setup_topics(){
  sprintf(irTopic, "%s/%s/%s", sub_prefix, device_name, ir_endpoint);
  sprintf(cfTopic, "%s/%s/%s", sub_prefix, device_name, config_endpoint);
  sprintf(pbTopic, "%s/%s", pub_prefix, device_name);
}

void setup_wifi(){
  wifi_station_set_hostname(device_name);
  AsyncWiFiManagerParameter custom_mqtt_server("server", "mqtt server", mqtt_server, 64);
  AsyncWiFiManagerParameter custom_mqtt_port("port", "port", mqtt_port, 9);
  AsyncWiFiManagerParameter custom_mqtt_username("username", "username", mqtt_username, 64);
  AsyncWiFiManagerParameter custom_mqtt_password("password", "password", "", 64);
  AsyncWiFiManagerParameter custom_device_name("deviceName", "device name", device_name, 64);

  AsyncWiFiManager wifiManager(&server, &dns);

  #ifndef DEBUG
    wifiManager.setDebugOutput(false);
  #endif
  wifiManager.setSaveConfigCallback(saveConfigCallback);
  wifiManager.setTimeout(300);
  wifiManager.addParameter(&custom_mqtt_server);
  wifiManager.addParameter(&custom_mqtt_port);
  wifiManager.addParameter(&custom_mqtt_username);
  wifiManager.addParameter(&custom_mqtt_password);
  wifiManager.addParameter(&custom_device_name);

  #ifdef DEBUG
    wifiManager.resetSettings();
  #endif

  if (!wifiManager.autoConnect(device_name, "ESP-8266")){
    Serial.println("failed to connect");
    delay(3000);
    ESP.reset();
    delay(5000);
  }

  Serial.println("Connected to ap");

  strcpy(mqtt_server, custom_mqtt_server.getValue());
  strcpy(mqtt_port, custom_mqtt_port.getValue());
  strcpy(mqtt_username, custom_mqtt_username.getValue());
  strcpy(device_name,   custom_device_name.getValue());
  if (!*custom_mqtt_password.getValue() == '\0'){
    strcpy(mqtt_password, custom_mqtt_password.getValue());
  }

  if (shouldSaveConfig){
    Serial.println("saving config");
    DynamicJsonBuffer jsonBuffer;
    JsonObject& json = jsonBuffer.createObject();

    json["mqtt_server"] = mqtt_server;
    json["mqtt_port"] = mqtt_port;
    json["mqtt_username"] = mqtt_username;
    json["mqtt_password"] = mqtt_password;
    json["device_name"] = device_name;

    File configFile = SPIFFS.open("/config.json", "w");
    if(!configFile){
      Serial.println("failed to open config file");
    }

    json.printTo(configFile);
    #ifdef DEBUG
      Serial.println("");
      json.prettyPrintTo(Serial);
      Serial.println("");
    #endif
    configFile.close();
  }

  Serial.print("IP: ");
  Serial.println(WiFi.localIP());
}

void setup_mqtt(){
  mqttClient.onConnect(onConnected);
  mqttClient.onMessage(onMessage);
  mqttClient.setServer(mqtt_server, atoi(mqtt_port)).setCredentials(mqtt_username, mqtt_password);
  mqttClient.connect();
}

void setup_config(){
  Serial.println("Mounting FS");
  if (SPIFFS.begin()){
    if (SPIFFS.exists("/config.json")){
      Serial.println("reading config file");
      File configFile = SPIFFS.open("/config.json", "r");

      if (configFile){
        Serial.println("opening config file");
        size_t size = configFile.size();
        std::unique_ptr<char[]> buf(new char[size]);

        configFile.readBytes(buf.get(), size);
        DynamicJsonBuffer jsonBuffer;
        JsonObject& json = jsonBuffer.parseObject(buf.get());
        #ifdef DEBUG
          Serial.println("");
          json.prettyPrintTo(Serial);
          Serial.println("");
        #endif
        if (json.success()){
          Serial.println("parsed json\n");
          strcpy(mqtt_server,   json["mqtt_server"]);
          strcpy(mqtt_port,     json["mqtt_port"]);
          strcpy(mqtt_username, json["mqtt_username"]);
          strcpy(mqtt_password, json["mqtt_password"]);
          strcpy(device_name,   json["device_name"]);
        }else{
          Serial.println("failed to load json config");
        }
      }
    }
  }else{
    Serial.println("failed to mount fs");
  }
}

void setup(){
  Serial.begin(115200);
  Serial.println("\r\n");
  #ifdef DEBUG
    Serial.println("Formatting SPIFFS");
    SPIFFS.format();
  #endif
  irRecv.enableIRIn();
  irSend.begin();

  setup_config();
  setup_wifi();
  setup_topics();
  setup_mqtt();

  ArduinoOTA.setHostname(device_name);
  ArduinoOTA.begin();
}

void ircode(decode_results *results, JsonObject& json){
  json["data"] = String(results->value, HEX);
  json["bits"] = String(results->bits, DEC);
}

void loop(){
  ArduinoOTA.handle();
  button.read();
  if (listen){
    if (irRecv.decode(&results)){
      if(results.value == 0xffffffff){
        irRecv.resume();
        return;
      }

      DynamicJsonBuffer jsonBuffer;
      JsonObject& root = jsonBuffer.createObject();
      String data;
      encodingToJson(&results, root);
      ircode(&results, root);
      root.prettyPrintTo(data);
      mqttClient.publish(pbTopic, 0, false, data.c_str());
      irRecv.resume();
    }
  }

  if (button.pressedFor(5000)){
    WiFi.disconnect(true);
    delay(1000);
    ESP.restart();
    delay(2000);
  }
}
