#define SUB_PREFIX "cmnd"
#define PUB_PREFIX "data"
#define IR_ENDPOINT "ir"
#define CONFIG_ENDPOINT "config"
#define DEVICE "WiFi-IR"
//#define DEBUG


//Hardware specifics
#ifdef NODE
  #define RECV_PIN D1
  #define TRAN_PIN D2
  #define BUTTON_PIN D0
#endif

#ifdef ESP01
  #define RECV_PIN 1
  #define TRAN_PIN 2
  #define BUTTON_PIN 3
#endif
